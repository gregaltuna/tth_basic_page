<?php
/**
 * @file
 * tth_basic_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function tth_basic_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_manual_css'.
  $permissions['create field_manual_css'] = array(
    'name' => 'create field_manual_css',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_manual_css'.
  $permissions['edit field_manual_css'] = array(
    'name' => 'edit field_manual_css',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_manual_css'.
  $permissions['edit own field_manual_css'] = array(
    'name' => 'edit own field_manual_css',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_manual_css'.
  $permissions['view field_manual_css'] = array(
    'name' => 'view field_manual_css',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_manual_css'.
  $permissions['view own field_manual_css'] = array(
    'name' => 'view own field_manual_css',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
