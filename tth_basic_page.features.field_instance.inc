<?php
/**
 * @file
 * tth_basic_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function tth_basic_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page-body'.
  $field_instances['node-page-body'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 10,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-page-field_adjust_page_title'.
  $field_instances['node-page-field_adjust_page_title'] = array(
    'bundle' => 'page',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_adjust_page_title',
    'label' => 'Adjust Page Title gutters',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 34,
    ),
  );

  // Exported field_instance: 'node-page-field_advanced_options'.
  $field_instances['node-page-field_advanced_options'] = array(
    'bundle' => 'page',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_advanced_options',
    'label' => 'Advanced Options',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-page-field_hide_page_title'.
  $field_instances['node-page-field_hide_page_title'] = array(
    'bundle' => 'page',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'For more complex layouts, such as if you\'re using the <a href="#">Hero Feature</a> functionality, you may wish to hide the Page Title as it naturally occurs at the top of the page. By selecting this option, the page title will not print on your page.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hide_page_title',
    'label' => 'Hide the Page Title for complex layouts',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'node-page-field_manual_css'.
  $field_instances['node-page-field_manual_css'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<p>If you need to affect something on this page with CSS, put it in this box. The code will only be available to this page, no other. <a href="http://www.w3schools.com/css/css3_intro.asp">www.w3schools.com</a> provides an excellent resource for CSS styles and attributes.</p>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_manual_css',
    'label' => 'Manual CSS',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-page-field_section_panel'.
  $field_instances['node-page-field_section_panel'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Use this "Section" field if you\'d like to separate your content into more manageable chunks. If you use this method, you can drag and drop the order of your sections around. This is best used with the "Remove the primary container..." option in the Advanced Layout Options tab, below.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_section_panel',
    'label' => 'Section Panel',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 10,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-page-field_wide_layout'.
  $field_instances['node-page-field_wide_layout'] = array(
    'bundle' => 'page',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'By selecting this option, you will be able to create full-width layouts, like the UNT home page. For more instructions on creating full-width layouts, <a href="#">click here</a>.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_wide_layout',
    'label' => 'Convert to Wide Layout',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 33,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('<p>If you need to affect something on this page with CSS, put it in this box. The code will only be available to this page, no other. <a href="http://www.w3schools.com/css/css3_intro.asp">www.w3schools.com</a> provides an excellent resource for CSS styles and attributes.</p>');
  t('Adjust Page Title gutters');
  t('Advanced Options');
  t('Body');
  t('By selecting this option, you will be able to create full-width layouts, like the UNT home page. For more instructions on creating full-width layouts, <a href="#">click here</a>.');
  t('Convert to Wide Layout');
  t('For more complex layouts, such as if you\'re using the <a href="#">Hero Feature</a> functionality, you may wish to hide the Page Title as it naturally occurs at the top of the page. By selecting this option, the page title will not print on your page.');
  t('Hide the Page Title for complex layouts');
  t('Manual CSS');
  t('Section Panel');
  t('Use this "Section" field if you\'d like to separate your content into more manageable chunks. If you use this method, you can drag and drop the order of your sections around. This is best used with the "Remove the primary container..." option in the Advanced Layout Options tab, below.');

  return $field_instances;
}
