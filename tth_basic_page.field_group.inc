<?php
/**
 * @file
 * tth_basic_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function tth_basic_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advanced_options|node|page|form';
  $field_group->group_name = 'group_advanced_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Advanced Layout Options',
    'weight' => '0',
    'children' => array(
      0 => 'field_wide_layout',
      1 => 'field_hide_page_title',
      2 => 'field_adjust_page_title',
      3 => 'field_advanced_options',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-advanced-options field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_advanced_options|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_manual_css|node|page|form';
  $field_group->group_name = 'group_manual_css';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Manual CSS',
    'weight' => '1',
    'children' => array(
      0 => 'field_manual_css',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-manual-css field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_manual_css|node|page|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Advanced Layout Options');
  t('Manual CSS');

  return $field_groups;
}
